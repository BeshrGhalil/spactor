#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of Spactor, The SMS spam detector.
# License: MIT, see the file "LICENSE" for details.

import pickle
from os import (getcwd, path)
from sys import argv
from sklearn.feature_extraction.text import TfidfVectorizer


def predict(sms, clf_path="/etc/spactor/clf.pkl", vec_path="/etc/spactor/vec.pkl"):
    """predict a tag for a sms string"""

    with open(clf_path, "rb") as file:
        model = pickle.load(file)

    with open(vec_path, "rb") as file:
        vec = pickle.load(file)

    vectorize_text = vec.transform([sms])
    prediction = model.predict(vectorize_text)[0]
    return prediction
