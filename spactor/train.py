#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of Spactor, The SMS spam detector.
# License: MIT, see the file "LICENSE" for details.

import pickle
import random
from os import getcwd, path

import numpy as np
from pandas import read_csv
from sklearn.calibration import *
from sklearn.dummy import *
from sklearn.ensemble import *
from sklearn.feature_extraction.text import (CountVectorizer,
                                             HashingVectorizer,
                                             TfidfVectorizer)
from sklearn.linear_model import *
from sklearn.multiclass import *
from sklearn.naive_bayes import *
from sklearn.neighbors import *
from sklearn.svm import *
from sklearn.tree import *


def perform(classifiers, vectorizers, train_data, test_data):
    scores = {}
    for vectorizer in vectorizers:
        features = vectorizer.fit_transform(train_data.text)
        for classifier in classifiers:
            if classifier.__class__.__name__ == "RidgeClassifierCV" and vectorizer.__class__.__name__ == "HashingVectorizer":
                continue
            # train
            classifier.fit(features, train_data.tag)

            # score
            vectorize_text = vectorizer.transform(test_data.text)
            score = classifier.score(vectorize_text, test_data.tag)
            string = "{}:{}:{}".format(classifier.__class__.__name__,
                                       vectorizer.__class__.__name__, score)
            scores[str(score)] = (score, classifier, vectorizer)
            print(string)

    scores_sorted = list(scores.keys())
    scores_sorted.sort(reverse=True)
    heighest_score = scores_sorted[0]
    print(
        "Heighest: clf: {clf:s}, vec: {vec:s}, score: {score:s}.{score_prec:s}%"
        .format(clf=scores[heighest_score][1].__class__.__name__,
                vec=scores[heighest_score][2].__class__.__name__,
                score=heighest_score[2:4],
                score_prec=heighest_score[4:6]), )
    print("-" * 10)
    return scores[heighest_score]


def train(dataset_path, output_path="/etc/spactor", stop_score=0.95, test_size=0.2):

    # select the fastest combination
    clf = None
    vec = None
    score = 0
    while score < stop_score:
        lines = []
        with open(dataset_path, "r") as fd:
            lines = fd.readlines()

        lines.pop(0)
        random.shuffle(lines)
        with open(dataset_path, "w") as fd:
            fd.write("tag;text\n")
            for line in lines:
                fd.write(line)

        data = read_csv(dataset_path, encoding='ISO-8859-1', sep=";")
        test_size = 0.1
        learn_items = int(len(data) - (len(data) * test_size))
        learn = data[:learn_items].fillna(" ")
        test = data[learn_items:].fillna(" ")

        score, clf, vec = perform([
            BernoulliNB(),
            AdaBoostClassifier(),
            BaggingClassifier(),
            ExtraTreesClassifier(),
            GradientBoostingClassifier(),
            DecisionTreeClassifier(),
            CalibratedClassifierCV(),
            DummyClassifier(),
            PassiveAggressiveClassifier(),
            RidgeClassifier(),
            RidgeClassifierCV(),
            SGDClassifier(),
            OneVsRestClassifier(SVC(kernel='linear')),
            OneVsRestClassifier(LogisticRegression()),
            KNeighborsClassifier()
        ], [
            CountVectorizer(),
            TfidfVectorizer(),
            HashingVectorizer()
        ], learn, test)

    # save the clf
    clf_filename = "clf.pkl"
    clf_filepath = path.join(output_path, clf_filename)
    with open(clf_filepath, "wb") as file:
        pickle.dump(clf, file)
    print("clf saved")

    # save the vectorizer
    vec_filename = "vec.pkl"
    vec_filepath = path.join(getcwd(), "models", vec_filename)
    with open(vec_filepath, "wb") as file:
        pickle.dump(vec, file)
    print("Vectorizer saved")

    return 0 
