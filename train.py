#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of Spactor, The SMS spam detector.
# License: MIT, see the file "LICENSE" for details.

from spactor.train import train
from sys import argv

if __name__ == "__main__":
    if len(argv) < 5:
        print(
            "Usage: python3 test.py <dataset> <output-path> <stop_score> <test-size>"
        )
        exit(1)

    dataset = argv[1]
    output = argv[2]
    try:
        stop_score = float(argv[3])
    except ValueError:
        print("stop-score must be an int value")
        exit(1)
    try:
        test_size = float(argv[4])
    except ValueError:
        print("test-size must be a float value")
        exit(1)
    train(dataset, output, stop_score, test_size)
