#!/usr/bin/env python
# This file is part of Spactor, The SMS spam detector.
# License: MIT, see the file "LICENCS" for details.import codecs
import codecs
import distutils.cmd
import os
import shutil

from setuptools import find_packages, setup

here = os.path.abspath(os.path.dirname(__file__))

with codecs.open(os.path.join(here, "README.md"), encoding="utf-8") as fh:
    long_description = "\n" + fh.read()

VERSION = '0.0.1'
DESCRIPTION = 'SMS spam detector'
LONG_DESCRIPTION = "English and Arabic SMS messages spam detector"


class InstallExec(distutils.cmd.Command):
    """A custom command to install spactor models."""

    description = 'install spactor models to path'
    user_options = list()

    def initialize_options(self):
        """Set default values for options."""
        pass

    def finalize_options(self):
        """Post-process options."""
        pass

    def run(self):
        """Run command."""
        here = os.path.abspath(os.path.dirname(__file__))

        dist_models_path = "/etc/spactor"
        if not os.path.lexists(dist_models_path):
            os.mkdir(dist_models_path)

        here_models_path = os.path.join(here, "models")
        if os.path.lexists(dist_models_path):
            shutil.rmtree(dist_models_path)
        shutil.copytree(here_models_path, dist_models_path)


# Setting up
setup(name="spactor",
      version=VERSION,
      author="Beshr Ghalil",
      author_email="<beshrghalil@protonmail.com>",
      description=DESCRIPTION,
      long_description_content_type="text/markdown",
      long_description=long_description,
      packages=find_packages(),
      url='https://github.com/BishrGhalil/spactor',
      install_requires=['sklearn', 'pandas'],
      keywords=['ml', 'machine', 'learning', 'sms', 'spam', 'detector'],
      classifiers=[
          'Environment :: Desktop',
          'Development Status :: 1 - Beta',
          'Intended Audience :: Developers',
          'Intended Audience :: End Users/Desktop',
          'License :: OSI Approved :: MIT',
          'Programming Language :: Python :: 3',
          'Operating System :: Unix',
          'Operating System :: POSIX',
          'Operating System :: MacOS :: MacOS X',
          'Topic :: Utilities',
      ],
      cmdclass={'install_exec': InstallExec})
