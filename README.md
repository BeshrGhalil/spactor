# Spactor
A Machine Learning based project for detecting spam SMS messages. It supports English and Arabic languages.

# Install
```
git clone https://github.com/BishrGhalil/spactor.git
cd spactor
sudo make install
```

# Test
```
python3 test.py
```

# Train
```
python3 train.py <dataset> <output-path> <stop_score> <test-size>
```
